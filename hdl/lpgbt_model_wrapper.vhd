-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.lpgbtfpga_package.all;
--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity lpgbt_model_wrapper is
    GENERIC (
         FEC                              : integer range 0 to 2;                  --! FEC selection can be: FEC5 or FEC12
         DATARATE                         : integer range 0 to 2 := DATARATE_5G12  --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12  		
    );	
    port(
        -- Ref clk
        refclk_i         : in  std_logic; -- In real-life applications, the lpGBT recovers the clock from the incoming serial stream
                                          -- For simulation purposes, we provide the reference clock here
		
        -- Reset
        rst_lpgbtrstn_i         : in  std_logic;

        -- Status
        stat_lpgbtrdy_o         : out std_logic;

        -- MGT
        dat_downLinkSerial_i    : in  std_logic;
        dat_upLinkSerial_o      : out std_logic;

        -- eClocks
        clk_eport0_o            : out std_logic;
        clk_eport1_o            : out std_logic;
        clk_eport2_o            : out std_logic;
        clk_eport3_o            : out std_logic;
        clk_eport4_o            : out std_logic;
        clk_eport5_o            : out std_logic;
        clk_eport6_o            : out std_logic;
        clk_eport7_o            : out std_logic;
        clk_eport8_o            : out std_logic;
        clk_eport9_o            : out std_logic;
        clk_eport10_o           : out std_logic;
        clk_eport11_o           : out std_logic;
        clk_eport12_o           : out std_logic;
        clk_eport13_o           : out std_logic;
        clk_eport14_o           : out std_logic;
        clk_eport15_o           : out std_logic;
        clk_eport16_o           : out std_logic;
        clk_eport17_o           : out std_logic;
        clk_eport18_o           : out std_logic;
        clk_eport19_o           : out std_logic;
        clk_eport20_o           : out std_logic;
        clk_eport21_o           : out std_logic;
        clk_eport22_o           : out std_logic;
        clk_eport23_o           : out std_logic;
        clk_eport24_o           : out std_logic;
        clk_eport25_o           : out std_logic;
        clk_eport26_o           : out std_logic;
        clk_eport27_o           : out std_logic;
        clk_eport28_o           : out std_logic;

        -- Downstream ePorts
        dat_eport0_o            : out std_logic;
        dat_eport1_o            : out std_logic;
        dat_eport2_o            : out std_logic;
        dat_eport3_o            : out std_logic;
        dat_eport10_o           : out std_logic;
        dat_eport11_o           : out std_logic;
        dat_eport12_o           : out std_logic;
        dat_eport13_o           : out std_logic;
        dat_eport20_o           : out std_logic;
        dat_eport21_o           : out std_logic;
        dat_eport22_o           : out std_logic;
        dat_eport23_o           : out std_logic;
        dat_eport30_o           : out std_logic;
        dat_eport31_o           : out std_logic;
        dat_eport32_o           : out std_logic;
        dat_eport33_o           : out std_logic;

        -- Upstream ePorts
        dat_eport0_i            : in  std_logic;
        dat_eport1_i            : in  std_logic;
        dat_eport2_i            : in  std_logic;
        dat_eport3_i            : in  std_logic;

        dat_eport10_i           : in  std_logic;
        dat_eport11_i           : in  std_logic;
        dat_eport12_i           : in  std_logic;
        dat_eport13_i           : in  std_logic;

        dat_eport20_i           : in  std_logic;
        dat_eport21_i           : in  std_logic;
        dat_eport22_i           : in  std_logic;
        dat_eport23_i           : in  std_logic;

        dat_eport30_i           : in  std_logic;
        dat_eport31_i           : in  std_logic;
        dat_eport32_i           : in  std_logic;
        dat_eport33_i           : in  std_logic;

        dat_eport40_i           : in  std_logic;
        dat_eport41_i           : in  std_logic;
        dat_eport42_i           : in  std_logic;
        dat_eport43_i           : in  std_logic;

        dat_eport50_i           : in  std_logic;
        dat_eport51_i           : in  std_logic;
        dat_eport52_i           : in  std_logic;
        dat_eport53_i           : in  std_logic;

        dat_eport60_i           : in  std_logic;
        dat_eport61_i           : in  std_logic;
        dat_eport62_i           : in  std_logic;
        dat_eport63_i           : in  std_logic;

        -- EC ePort
        dat_ec_o                : out std_logic;
        dat_ec_i                : in  std_logic;

        -- Phase shifted clocks
        clk_psclk0_o            : out std_logic;
        clk_psclk1_o            : out std_logic;
        clk_psclk2_o            : out std_logic;
        clk_psclk3_o            : out std_logic;

        -- Slow control interfaces (IC)
        dat_i2cm0_sda           : inout std_logic;
        clk_i2cm0_scl           : inout std_logic;

        dat_i2cm1_sda           : inout std_logic;
        clk_i2cm1_scl           : inout std_logic;

        dat_i2cm2_sda           : inout std_logic;
        clk_i2cm2_scl           : inout std_logic;

        dat_gpio0_io            : inout std_logic;
        dat_gpio1_io            : inout std_logic;
        dat_gpio2_io            : inout std_logic;
        dat_gpio3_io            : inout std_logic;
        dat_gpio4_io            : inout std_logic;
        dat_gpio5_io            : inout std_logic;
        dat_gpio6_io            : inout std_logic;
        dat_gpio7_io            : inout std_logic;
        dat_gpio8_io            : inout std_logic;
        dat_gpio9_io            : inout std_logic;
        dat_gpio10_io           : inout std_logic;
        dat_gpio11_io           : inout std_logic;
        dat_gpio12_io           : inout std_logic;
        dat_gpio13_io           : inout std_logic;
        dat_gpio14_io           : inout std_logic;
        dat_gpio15_io           : inout std_logic
    );
end lpgbt_model_wrapper;

--=================================================================================================--
--####################################   Architecture   ###########################################--
--=================================================================================================--

architecture behabioral of lpgbt_model_wrapper is

    COMPONENT lpGBT
        GENERIC(
            FUSE0x00_CHIPID0                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x01_CHIPID1                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x02_CHIPID2                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x03_CHIPID3                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x04_USERID0                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x05_USERID1                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x06_USERID2                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x07_USERID3                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x08_DACCAL0                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x09_DACCAL1                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x0A_DACCAL2                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x0B_ADCCAL0                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x0C_ADCCAL1                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x0D_ADCCAL2                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x0E_ADCCAL3                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x0F_ADCCAL4                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x10_ADCCAL5                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x11_ADCCAL6                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x12_ADCCAL7                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x13_ADCCAL8                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x14_ADCCAL9                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x15_ADCCAL10                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x16_ADCCAL11                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x17_ADCCAL12                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x18_ADCCAL13                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x19_ADCCAL14                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x1A_TEMPCALH                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x1B_TEMPCALL                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x1C_VREFCNTR                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x1D_CURDACCALH                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x1E_CURDACCALL                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x1F_RESERVED0                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x20_CLKGCONFIG0                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x21_CLKGCONFIG1                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x22_CLKGPLLRES                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x23_CLKGPLLINTCUR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x24_CLKGPLLPROPCUR                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x25_CLKGCDRPROPCUR                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x26_CLKGCDRINTCUR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x27_CLKGCDRFFPROPCUR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0x28_CLKGFLLINTCUR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x29_CLKGFFCAP                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x2A_CLKGCNTOVERRIDE                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x2B_CLKGOVERRIDECAPBANK                : std_logic_vector(7 downto 0) := x"00";
            FUSE0x2C_CLKGWAITTIME                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x2D_CLKGLFCONFIG0                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x2E_CLKGLFCONFIG1                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x2F_FAMAXHEADERFOUNDCOUNT              : std_logic_vector(7 downto 0) := x"00";
            FUSE0x30_FAMAXHEADERFOUNDCOUNTAFTERNF       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x31_FAMAXHEADERNOTFOUNDCOUNT           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x32_FAFAMAXSKIPCYCLECOUNTAFTERNF       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x33_PSDLLCONFIG                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x34_EPRXDLLCONFIG                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x35_FORCEENABLE                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x36_CHIPADR                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x37_EQCONFIG                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x38_EQRES                              : std_logic_vector(7 downto 0) := x"00";
            FUSE0x39_LDCONFIGH                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x3A_LDCONFIGL                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x3B_REFCLK                             : std_logic_vector(7 downto 0) := x"00";
            FUSE0x3C_SCCONFIG                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x3D_RESETCONFIG                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x3E_PGCONFIG                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x3F_I2CMTRANSCONFIG                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x40_I2CMTRANSADDRESS                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0x41_I2CMTRANSCTRL                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0x42_I2CMTRANSDATA0                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x43_I2CMTRANSDATA1                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x44_I2CMTRANSDATA2                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x45_I2CMTRANSDATA3                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x46_I2CMTRANSDATA4                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x47_I2CMTRANSDATA5                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x48_I2CMTRANSDATA6                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x49_I2CMTRANSDATA7                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x4A_I2CMTRANSDATA8                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x4B_I2CMTRANSDATA9                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x4C_I2CMTRANSDATA10                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x4D_I2CMTRANSDATA11                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x4E_I2CMTRANSDATA12                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x4F_I2CMTRANSDATA13                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x50_I2CMTRANSDATA14                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x51_I2CMTRANSDATA15                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x52_PIODIRH                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x53_PIODIRL                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x54_PIOOUTH                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x55_PIOOUTL                            : std_logic_vector(7 downto 0) := x"00";
            FUSE0x56_PIOPULLENAH                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x57_PIOPULLENAL                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x58_PIOUPDOWNH                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x59_PIOUPDOWNL                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x5A_PIODRIVESTRENGTHH                  : std_logic_vector(7 downto 0) := x"00";
            FUSE0x5B_PIODRIVESTRENGTHL                  : std_logic_vector(7 downto 0) := x"00";
            FUSE0x5C_PS0CONFIG                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x5D_PS0DELAY                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x5E_PS0OUTDRIVER                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x5F_PS1CONFIG                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x60_PS1DELAY                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x61_PS1OUTDRIVER                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x62_PS2CONFIG                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x63_PS2DELAY                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x64_PS2OUTDRIVER                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x65_PS3CONFIG                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x66_PS3DELAY                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0x67_PS3OUTDRIVER                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0x68_DACCONFIGH                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x69_DACCONFIGL                         : std_logic_vector(7 downto 0) := x"00";
            FUSE0x6A_CURDACVALUE                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0x6B_CURDACCHN                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0x6C_EPCLK0CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x6D_EPCLK0CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x6E_EPCLK1CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x6F_EPCLK1CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x70_EPCLK2CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x71_EPCLK2CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x72_EPCLK3CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x73_EPCLK3CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x74_EPCLK4CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x75_EPCLK4CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x76_EPCLK5CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x77_EPCLK5CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x78_EPCLK6CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x79_EPCLK6CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x7A_EPCLK7CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x7B_EPCLK7CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x7C_EPCLK8CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x7D_EPCLK8CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x7E_EPCLK9CHNCNTRH                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x7F_EPCLK9CHNCNTRL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0x80_EPCLK10CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x81_EPCLK10CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x82_EPCLK11CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x83_EPCLK11CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x84_EPCLK12CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x85_EPCLK12CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x86_EPCLK13CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x87_EPCLK13CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x88_EPCLK14CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x89_EPCLK14CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x8A_EPCLK15CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x8B_EPCLK15CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x8C_EPCLK16CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x8D_EPCLK16CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x8E_EPCLK17CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x8F_EPCLK17CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x90_EPCLK18CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x91_EPCLK18CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x92_EPCLK19CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x93_EPCLK19CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x94_EPCLK20CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x95_EPCLK20CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x96_EPCLK21CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x97_EPCLK21CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x98_EPCLK22CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x99_EPCLK22CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x9A_EPCLK23CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x9B_EPCLK23CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x9C_EPCLK24CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x9D_EPCLK24CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x9E_EPCLK25CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0x9F_EPCLK25CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA0_EPCLK26CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA1_EPCLK26CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA2_EPCLK27CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA3_EPCLK27CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA4_EPCLK28CHNCNTRH                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA5_EPCLK28CHNCNTRL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA6_RESERVED1                          : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA7_EPTXDATARATE                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA8_EPTXCONTROL                        : std_logic_vector(7 downto 0) := x"00";
            FUSE0xA9_EPTX10ENABLE                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xAA_EPTX32ENABLE                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xAB_EPTXECCHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xAC_EPTX00CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xAD_EPTX01CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xAE_EPTX02CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xAF_EPTX03CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB0_EPTX10CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB1_EPTX11CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB2_EPTX12CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB3_EPTX13CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB4_EPTX20CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB5_EPTX21CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB6_EPTX22CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB7_EPTX23CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB8_EPTX30CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xB9_EPTX31CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xBA_EPTX32CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xBB_EPTX33CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xBC_EPTX01_00CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xBD_EPTX03_02CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xBE_EPTX11_10CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xBF_EPTX13_12CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC0_EPTX21_20CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC1_EPTX23_22CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC2_EPTX31_30CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC3_EPTX33_32CHNCNTR                   : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC4_EPRX0CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC5_EPRX1CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC6_EPRX2CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC7_EPRX3CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC8_EPRX4CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xC9_EPRX5CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xCA_EPRX6CONTROL                       : std_logic_vector(7 downto 0) := x"00";
            FUSE0xCB_EPRXECCONTROL                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xCC_EPRX00CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xCD_EPRX01CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xCE_EPRX02CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xCF_EPRX03CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD0_EPRX10CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD1_EPRX11CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD2_EPRX12CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD3_EPRX13CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD4_EPRX20CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD5_EPRX21CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD6_EPRX22CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD7_EPRX23CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD8_EPRX30CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xD9_EPRX31CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xDA_EPRX32CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xDB_EPRX33CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xDC_EPRX40CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xDD_EPRX41CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xDE_EPRX42CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xDF_EPRX43CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE0_EPRX50CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE1_EPRX51CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE2_EPRX52CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE3_EPRX53CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE4_EPRX60CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE5_EPRX61CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE6_EPRX62CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE7_EPRX63CHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE8_EPRXECCHNCNTR                      : std_logic_vector(7 downto 0) := x"00";
            FUSE0xE9_EPRXEQ10CONTROL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xEA_EPRXEQ32CONTROL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xEB_EPRXEQ54CONTROL                    : std_logic_vector(7 downto 0) := x"00";
            FUSE0xEC_EPRXEQ6CONTROL                     : std_logic_vector(7 downto 0) := x"00";
            FUSE0xED_POWERUP0                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0xEE_POWERUP1                           : std_logic_vector(7 downto 0) := x"00";
            FUSE0xEF_POWERUP2                           : std_logic_vector(7 downto 0) := x"00"
        );
        PORT (
            -- CORE and IO power supply
            GND                  : in  std_logic;
            VDD1V2               : in  std_logic;

            -- Transmitter power supply
            GNDTX                : in  std_logic;
            VDDTX1V2             : in  std_logic;

            -- Receiver power supply
            GNDRX                : in  std_logic;
            VDDRX1V2             : in  std_logic;

            -- Analog power supply
            GNDA                 : in  std_logic;
            VDDA1V2              : in  std_logic;

            -- Fuses power supply (uses GND for return currents)
            VDDF2V5              : in  std_logic;

            -- High speed serializer outputs
            HSOUTP               : out std_logic;
            HSOUTN               : out std_logic;

            -- High speed deserializer inputs
            HSINP                : in  std_logic;
            HSINN                : in  std_logic;

            -- ePort clock differential outputs
            ECLK0P               : out std_logic;
            ECLK0N               : out std_logic;

            ECLK1P               : out std_logic;
            ECLK1N               : out std_logic;

            ECLK2P               : out std_logic;
            ECLK2N               : out std_logic;

            ECLK3P               : out std_logic;
            ECLK3N               : out std_logic;

            ECLK4P               : out std_logic;
            ECLK4N               : out std_logic;

            ECLK5P               : out std_logic;
            ECLK5N               : out std_logic;

            ECLK6P               : out std_logic;
            ECLK6N               : out std_logic;

            ECLK7P               : out std_logic;
            ECLK7N               : out std_logic;

            ECLK8P               : out std_logic;
            ECLK8N               : out std_logic;

            ECLK9P               : out std_logic;
            ECLK9N               : out std_logic;

            ECLK10P              : out std_logic;
            ECLK10N              : out std_logic;

            ECLK11P              : out std_logic;
            ECLK11N              : out std_logic;

            ECLK12P              : out std_logic;
            ECLK12N              : out std_logic;

            ECLK13P              : out std_logic;
            ECLK13N              : out std_logic;

            ECLK14P              : out std_logic;
            ECLK14N              : out std_logic;

            ECLK15P              : out std_logic;
            ECLK15N              : out std_logic;

            ECLK16P              : out std_logic;
            ECLK16N              : out std_logic;

            ECLK17P              : out std_logic;
            ECLK17N              : out std_logic;

            ECLK18P              : out std_logic;
            ECLK18N              : out std_logic;

            ECLK19P              : out std_logic;
            ECLK19N              : out std_logic;

            ECLK20P              : out std_logic;
            ECLK20N              : out std_logic;

            ECLK21P              : out std_logic;
            ECLK21N              : out std_logic;

            ECLK22P              : out std_logic;
            ECLK22N              : out std_logic;

            ECLK23P              : out std_logic;
            ECLK23N              : out std_logic;

            ECLK24P              : out std_logic;
            ECLK24N              : out std_logic;

            ECLK25P              : out std_logic;
            ECLK25N              : out std_logic;

            ECLK26P              : out std_logic;
            ECLK26N              : out std_logic;

            ECLK27P              : out std_logic;
            ECLK27N              : out std_logic;

            ECLK28P              : out std_logic;
            ECLK28N              : out std_logic;

            -- ePortTX group 0 differential data outputs (downlink)
            EDOUT00P             : out std_logic;
            EDOUT00N             : out std_logic;
            EDOUT01P             : out std_logic;
            EDOUT01N             : out std_logic;
            EDOUT02P             : out std_logic;
            EDOUT02N             : out std_logic;
            EDOUT03P             : out std_logic;
            EDOUT03N             : out std_logic;

            -- ePortTX group 1 differential data outputs (downlink)
            EDOUT10P             : out std_logic;
            EDOUT10N             : out std_logic;
            EDOUT11P             : out std_logic;
            EDOUT11N             : out std_logic;
            EDOUT12P             : out std_logic;
            EDOUT12N             : out std_logic;
            EDOUT13P             : out std_logic;
            EDOUT13N             : out std_logic;

            -- ePortTX group 2 differential data outputs (downlink)
            EDOUT20P             : out std_logic;
            EDOUT20N             : out std_logic;
            EDOUT21P             : out std_logic;
            EDOUT21N             : out std_logic;
            EDOUT22P             : out std_logic;
            EDOUT22N             : out std_logic;
            EDOUT23P             : out std_logic;
            EDOUT23N             : out std_logic;

            -- ePortTX group 3 differential data outputs (downlink)
            EDOUT30P             : out std_logic;
            EDOUT30N             : out std_logic;
            EDOUT31P             : out std_logic;
            EDOUT31N             : out std_logic;
            EDOUT32P             : out std_logic;
            EDOUT32N             : out std_logic;
            EDOUT33P             : out std_logic;
            EDOUT33N             : out std_logic;

            -- ePortTX EC differential data outputs
            EDOUTECP             : out std_logic;
            EDOUTECN             : out std_logic;

            -- ePortRX group 0 differential data inputs (uplink)
            EDIN00P              : in  std_logic;
            EDIN00N              : in  std_logic;
            EDIN01P              : in  std_logic;
            EDIN01N              : in  std_logic;
            EDIN02P              : in  std_logic;
            EDIN02N              : in  std_logic;
            EDIN03P              : in  std_logic;
            EDIN03N              : in  std_logic;

            -- ePortRX group 1 differential data inputs (uplink)
            EDIN10P              : in  std_logic;
            EDIN10N              : in  std_logic;
            EDIN11P              : in  std_logic;
            EDIN11N              : in  std_logic;
            EDIN12P              : in  std_logic;
            EDIN12N              : in  std_logic;
            EDIN13P              : in  std_logic;
            EDIN13N              : in  std_logic;

            -- ePortRX group 2 differential data inputs (uplink)
            EDIN20P              : in  std_logic;
            EDIN20N              : in  std_logic;
            EDIN21P              : in  std_logic;
            EDIN21N              : in  std_logic;
            EDIN22P              : in  std_logic;
            EDIN22N              : in  std_logic;
            EDIN23P              : in  std_logic;
            EDIN23N              : in  std_logic;

            -- ePortRX group 3 differential data inputs (uplink)
            EDIN30P              : in  std_logic;
            EDIN30N              : in  std_logic;
            EDIN31P              : in  std_logic;
            EDIN31N              : in  std_logic;
            EDIN32P              : in  std_logic;
            EDIN32N              : in  std_logic;
            EDIN33P              : in  std_logic;
            EDIN33N              : in  std_logic;

            -- ePortRX group 4 differential data inputs (uplink)
            EDIN40P              : in  std_logic;
            EDIN40N              : in  std_logic;
            EDIN41P              : in  std_logic;
            EDIN41N              : in  std_logic;
            EDIN42P              : in  std_logic;
            EDIN42N              : in  std_logic;
            EDIN43P              : in  std_logic;
            EDIN43N              : in  std_logic;

            -- ePortRX group 5 differential data inputs (uplink)
            EDIN50P              : in  std_logic;
            EDIN50N              : in  std_logic;
            EDIN51P              : in  std_logic;
            EDIN51N              : in  std_logic;
            EDIN52P              : in  std_logic;
            EDIN52N              : in  std_logic;
            EDIN53P              : in  std_logic;
            EDIN53N              : in  std_logic;

            -- ePortRX group 6 differential data inputs (uplink)
            EDIN60P              : in  std_logic;
            EDIN60N              : in  std_logic;
            EDIN61P              : in  std_logic;
            EDIN61N              : in  std_logic;
            EDIN62P              : in  std_logic;
            EDIN62N              : in  std_logic;
            EDIN63P              : in  std_logic;
            EDIN63N              : in  std_logic;

            -- ePortRX EC differential data inputs
            EDINECP              : in  std_logic;
            EDINECN              : in  std_logic;

            -- Phase shifted clocks
            PSCLK0P              : out std_logic;
            PSCLK0N              : out std_logic;
            PSCLK1P              : out std_logic;
            PSCLK1N              : out std_logic;
            PSCLK2P              : out std_logic;
            PSCLK2N              : out std_logic;
            PSCLK3P              : out std_logic;
            PSCLK3N              : out std_logic;

            -- I2C slave for ASIC control
            SLSDA                : inout std_logic;
            SLSCL                : inout std_logic;

            -- Address
            ADR0                 : in  std_logic;
            ADR1                 : in  std_logic;
            ADR2                 : in  std_logic;
            ADR3                 : in  std_logic;

            -- lock mode
            LOCKMODE             : in  std_logic;

            -- reset (active low)
            RSTB                 : in  std_logic;

            -- reset signal
            RSTOUTB              : out std_logic;

            -- mode of operation
            MODE0                : in  std_logic;
            MODE1                : in  std_logic;
            MODE2                : in  std_logic;
            MODE3                : in  std_logic;

            -- lpGBT Ready signal
            READY                : out std_logic;

            -- Power On Reset Disable
            PORDIS               : in  std_logic;

            -- reference clock
            REFCLKP              : in  std_logic;
            REFCLKN              : in  std_logic;

            -- Test clock (used only for debugging)
            TSTCLKINP            : in  std_logic;
            TSTCLKINN            : in  std_logic;

            -- Bypass VCO and use test clock (TSTCLKIN) instead
            VCOBYPASS            : in  std_logic;

            -- State override for the power up state machine
            STATEOVRD            : in  std_logic;

            -- Selects configuration interface (0=SerialControll1=Slave I2C)
            SC_I2C               : in  std_logic;

            -- Test outputs (0-3 CMOS 4-5 differential)
            TSTOUT0              : out std_logic;
            TSTOUT1              : out std_logic;
            TSTOUT2              : out std_logic;
            TSTOUT3              : out std_logic;
            TSTOUT4P             : out std_logic;
            TSTOUT4N             : out std_logic;
            TSTOUT5P             : out std_logic;
            TSTOUT5N             : out std_logic;

            -- I2C Master 0 signals
            M0SDA                : inout std_logic;
            M0SCL                : inout std_logic;

            -- I2C Master 1 signals
            M1SDA                : inout std_logic;
            M1SCL                : inout std_logic;

            -- I2C Master 2 signals
            M2SDA                : inout std_logic;
            M2SCL                : inout std_logic;

            -- Parallel I/O
            GPIO0                : inout std_logic;
            GPIO1                : inout std_logic;
            GPIO2                : inout std_logic;
            GPIO3                : inout std_logic;
            GPIO4                : inout std_logic;
            GPIO5                : inout std_logic;
            GPIO6                : inout std_logic;
            GPIO7                : inout std_logic;
            GPIO8                : inout std_logic;
            GPIO9                : inout std_logic;
            GPIO10               : inout std_logic;
            GPIO11               : inout std_logic;
            GPIO12               : inout std_logic;
            GPIO13               : inout std_logic;
            GPIO14               : inout std_logic;
            GPIO15               : inout std_logic;

            -- ADC (and current source output)
            ADC0                 : inout std_logic;
            ADC1                 : inout std_logic;
            ADC2                 : inout std_logic;
            ADC3                 : inout std_logic;
            ADC4                 : inout std_logic;
            ADC5                 : inout std_logic;
            ADC6                 : inout std_logic;
            ADC7                 : inout std_logic;

            -- Voltage DAC output
            VDAC                 : out std_logic;

            -- reference voltage
            VREF                 : inout std_logic;

            -- debug signals (not present on the chip package)
            debug_registers     : out std_logic_vector(463*8-1 downto 0);
            debug_testOutputs   : out std_logic_vector(127 downto 0)
        );
    END COMPONENT;

    -- lpGBT modes of operation
    constant LPGBT_5G_FEC5_OFF      : std_logic_vector(3 downto 0) := "0000";
    constant LPGBT_5G_FEC5_TX       : std_logic_vector(3 downto 0) := "0001";
    constant LPGBT_5G_FEC5_RX       : std_logic_vector(3 downto 0) := "0010";
    constant LPGBT_5G_FEC5_TRX      : std_logic_vector(3 downto 0) := "0011";

    constant LPGBT_5G_FEC12_OFF     : std_logic_vector(3 downto 0) := "0100";
    constant LPGBT_5G_FEC12_TX      : std_logic_vector(3 downto 0) := "0101";
    constant LPGBT_5G_FEC12_RX      : std_logic_vector(3 downto 0) := "0110";
    constant LPGBT_5G_FEC12_TRX     : std_logic_vector(3 downto 0) := "0111";

    constant LPGBT_10G_FEC5_OFF     : std_logic_vector(3 downto 0) := "1000";
    constant LPGBT_10G_FEC5_TX      : std_logic_vector(3 downto 0) := "1001";
    constant LPGBT_10G_FEC5_RX      : std_logic_vector(3 downto 0) := "1010";
    constant LPGBT_10G_FEC5_TRX     : std_logic_vector(3 downto 0) := "1011";

    constant LPGBT_10G_FEC12_OFF    : std_logic_vector(3 downto 0) := "1100";
    constant LPGBT_10G_FEC12_TX     : std_logic_vector(3 downto 0) := "1101";
    constant LPGBT_10G_FEC12_RX     : std_logic_vector(3 downto 0) := "1110";
    constant LPGBT_10G_FEC12_TRX    : std_logic_vector(3 downto 0) := "1111";

    -- lpGBT ID
    constant MYID                   : std_logic_vector(31 downto 0) := x"76543210";

    -- Signals
    signal refclk_n                 : std_logic;	
    signal dat_downLinkSerial_n_s   : std_logic;

    signal dat_eport0_n_s           : std_logic;
    signal dat_eport1_n_s           : std_logic;
    signal dat_eport2_n_s           : std_logic;
    signal dat_eport3_n_s           : std_logic;

    signal dat_eport10_n_s          : std_logic;
    signal dat_eport11_n_s          : std_logic;
    signal dat_eport12_n_s          : std_logic;
    signal dat_eport13_n_s          : std_logic;

    signal dat_eport20_n_s          : std_logic;
    signal dat_eport21_n_s          : std_logic;
    signal dat_eport22_n_s          : std_logic;
    signal dat_eport23_n_s          : std_logic;

    signal dat_eport30_n_s          : std_logic;
    signal dat_eport31_n_s          : std_logic;
    signal dat_eport32_n_s          : std_logic;
    signal dat_eport33_n_s          : std_logic;

    signal dat_eport40_n_s          : std_logic;
    signal dat_eport41_n_s          : std_logic;
    signal dat_eport42_n_s          : std_logic;
    signal dat_eport43_n_s          : std_logic;

    signal dat_eport50_n_s          : std_logic;
    signal dat_eport51_n_s          : std_logic;
    signal dat_eport52_n_s          : std_logic;
    signal dat_eport53_n_s          : std_logic;

    signal dat_eport60_n_s          : std_logic;
    signal dat_eport61_n_s          : std_logic;
    signal dat_eport62_n_s          : std_logic;
    signal dat_eport63_n_s          : std_logic;

    signal dat_ec_n_s               : std_logic;

    signal conf_mode_s              : std_logic_vector(3 downto 0);

    signal SLSDA_s                  : std_logic;
    signal SLSCL_s                  : std_logic;

    signal dat_ec_tosca_s           : std_logic;
    signal dat_ec_fromsca_s         : std_logic;

    signal clk_eport28_s            : std_logic;

begin                 --========####   Architecture Body   ####========--

    -- Mode configuration
    conf_mode_s              <= LPGBT_5G_FEC12_TRX  when DATARATE = DATARATE_5G12 and FEC = FEC12 else
                                LPGBT_5G_FEC5_TRX   when DATARATE = DATARATE_5G12 and FEC = FEC5 else
                                LPGBT_10G_FEC5_TRX  when DATARATE = DATARATE_10G24 and FEC = FEC5 else
                                LPGBT_10G_FEC12_TRX;

    SLSDA_s <= '1';
    SLSCL_s <= '1';

    -- Negative signals association
    dat_downLinkSerial_n_s   <= not(dat_downLinkSerial_i);

    dat_eport0_n_s           <= not (dat_eport0_i);
    dat_eport1_n_s           <= not (dat_eport1_i);
    dat_eport2_n_s           <= not (dat_eport2_i);
    dat_eport3_n_s           <= not (dat_eport3_i);

    dat_eport10_n_s          <= not (dat_eport10_i);
    dat_eport11_n_s          <= not (dat_eport11_i);
    dat_eport12_n_s          <= not (dat_eport12_i);
    dat_eport13_n_s          <= not (dat_eport13_i);

    dat_eport20_n_s          <= not (dat_eport20_i);
    dat_eport21_n_s          <= not (dat_eport21_i);
    dat_eport22_n_s          <= not (dat_eport22_i);
    dat_eport23_n_s          <= not (dat_eport23_i);

    dat_eport30_n_s          <= not (dat_eport30_i);
    dat_eport31_n_s          <= not (dat_eport31_i);
    dat_eport32_n_s          <= not (dat_eport32_i);
    dat_eport33_n_s          <= not (dat_eport33_i);

    dat_eport40_n_s          <= not (dat_eport40_i);
    dat_eport41_n_s          <= not (dat_eport41_i);
    dat_eport42_n_s          <= not (dat_eport42_i);
    dat_eport43_n_s          <= not (dat_eport43_i);

    dat_eport50_n_s          <= not (dat_eport50_i);
    dat_eport51_n_s          <= not (dat_eport51_i);
    dat_eport52_n_s          <= not (dat_eport52_i);
    dat_eport53_n_s          <= not (dat_eport53_i);

    dat_eport60_n_s          <= not (dat_eport60_i);
    dat_eport61_n_s          <= not (dat_eport61_i);
    dat_eport62_n_s          <= not (dat_eport62_i);
    dat_eport63_n_s          <= not (dat_eport63_i);

    dat_ec_n_s               <= not (dat_ec_fromsca_s);

    clk_eport28_o            <= clk_eport28_s;

refclk_n <= not refclk_i;

    LpGBT_Model_inst: lpGBT
        GENERIC MAP(
            FUSE0x00_CHIPID0          => x"10",
            FUSE0x01_CHIPID1          => x"32",
            FUSE0x02_CHIPID2          => x"54",
            FUSE0x03_CHIPID3          => x"76",
            FUSE0x20_CLKGCONFIG0      => x"88",
            FUSE0x21_CLKGCONFIG1      => x"24",
            FUSE0x23_CLKGPLLINTCUR    => x"55",
            FUSE0x24_CLKGPLLPROPCUR   => x"55",
            FUSE0x22_CLKGPLLRES       => x"44",
            FUSE0x29_CLKGFFCAP        => x"36",
            FUSE0x26_CLKGCDRINTCUR    => x"55",
            FUSE0x28_CLKGFLLINTCUR    => x"0f",
            FUSE0x25_CLKGCDRPROPCUR   => x"55",
            FUSE0x27_CLKGCDRFFPROPCUR => x"55",
            FUSE0x2D_CLKGLFCONFIG0    => x"89",
            FUSE0x2E_CLKGLFCONFIG1    => x"99",
            FUSE0x2C_CLKGWAITTIME     => x"aa",
            FUSE0x6C_EPCLK0CHNCNTRH   => x"0b",
            FUSE0x6E_EPCLK1CHNCNTRH   => x"0b",
            FUSE0x70_EPCLK2CHNCNTRH   => x"0b",
            FUSE0x72_EPCLK3CHNCNTRH   => x"0b",
            FUSE0x74_EPCLK4CHNCNTRH   => x"0b",
            FUSE0x76_EPCLK5CHNCNTRH   => x"0b",
            FUSE0x78_EPCLK6CHNCNTRH   => x"0b",
            FUSE0x7A_EPCLK7CHNCNTRH   => x"0b",
            FUSE0x7C_EPCLK8CHNCNTRH   => x"0b",
            FUSE0x7E_EPCLK9CHNCNTRH   => x"0b",
            FUSE0x80_EPCLK10CHNCNTRH  => x"0b",
            FUSE0x82_EPCLK11CHNCNTRH  => x"0b",
            FUSE0x84_EPCLK12CHNCNTRH  => x"0b",
            FUSE0x86_EPCLK13CHNCNTRH  => x"0b",
            FUSE0x88_EPCLK14CHNCNTRH  => x"0b",
            FUSE0x8A_EPCLK15CHNCNTRH  => x"0b",
            FUSE0x8C_EPCLK16CHNCNTRH  => x"0b",
            FUSE0x8E_EPCLK17CHNCNTRH  => x"0b",
            FUSE0x90_EPCLK18CHNCNTRH  => x"0b",
            FUSE0x92_EPCLK19CHNCNTRH  => x"0b",
            FUSE0x94_EPCLK20CHNCNTRH  => x"0b",
            FUSE0x96_EPCLK21CHNCNTRH  => x"0b",
            FUSE0x98_EPCLK22CHNCNTRH  => x"0b",
            FUSE0x9A_EPCLK23CHNCNTRH  => x"0b",
            FUSE0x9C_EPCLK24CHNCNTRH  => x"0b",
            FUSE0x9E_EPCLK25CHNCNTRH  => x"0b",
            FUSE0xA0_EPCLK26CHNCNTRH  => x"0b",
            FUSE0xA2_EPCLK27CHNCNTRH  => x"0b",
            FUSE0xA4_EPCLK28CHNCNTRH  => x"09", --40MHz clock
            FUSE0xC4_EPRX0CONTROL     => x"fe",
            FUSE0xC5_EPRX1CONTROL     => x"fe",
            FUSE0xC6_EPRX2CONTROL     => x"fe",
            FUSE0xC7_EPRX3CONTROL     => x"fe",
            FUSE0xC8_EPRX4CONTROL     => x"fe",
            FUSE0xC9_EPRX5CONTROL     => x"fe",
            FUSE0xCA_EPRX6CONTROL     => x"fe",
            FUSE0xCB_EPRXECCONTROL    => x"02",
            FUSE0xA7_EPTXDATARATE     => x"ff", -- x55 => 80Mbps on each e-groups
            FUSE0xA9_EPTX10ENABLE     => x"ff",
            FUSE0xAA_EPTX32ENABLE     => x"ff",

            FUSE0xA8_EPTXCONTROL      => x"10",
            FUSE0xAB_EPTXECCHNCNTR    => x"01",

            FUSE0xAC_EPTX00CHNCNTR    => x"01", -- x"04" to increase the amplitude
            FUSE0xAD_EPTX01CHNCNTR    => x"01",
            FUSE0xAE_EPTX02CHNCNTR    => x"01",
            FUSE0xAF_EPTX03CHNCNTR    => x"01",
            FUSE0xB0_EPTX10CHNCNTR    => x"01",
            FUSE0xB1_EPTX11CHNCNTR    => x"01",
            FUSE0xB2_EPTX12CHNCNTR    => x"01",
            FUSE0xB3_EPTX13CHNCNTR    => x"01",
            FUSE0xB4_EPTX20CHNCNTR    => x"01",
            FUSE0xB5_EPTX21CHNCNTR    => x"01",
            FUSE0xB6_EPTX22CHNCNTR    => x"01",
            FUSE0xB7_EPTX23CHNCNTR    => x"01",
            FUSE0xB8_EPTX30CHNCNTR    => x"01",
            FUSE0xB9_EPTX31CHNCNTR    => x"01",
            FUSE0xBA_EPTX32CHNCNTR    => x"01",
            FUSE0xBB_EPTX33CHNCNTR    => x"01",

            FUSE0xE8_EPRXECCHNCNTR    => x"01",

            FUSE0x39_LDCONFIGH        => x"01",
            FUSE0xED_POWERUP0         => x"00",
            FUSE0xEF_POWERUP2         => x"07"
        )
        PORT MAP(
            -- CORE and IO power supply
            GND                  => '0',
            VDD1V2               => '1',

            -- Transmitter power supply
            GNDTX                => '0',
            VDDTX1V2             => '1',

            -- Receiver power supply
            GNDRX                => '0',
            VDDRX1V2             => '1',

            -- Analog power supply
            GNDA                 => '0',
            VDDA1V2              => '1',

            -- Fuses power supply (uses GND for return currents)
            VDDF2V5              => '0',

            -- High speed serializer outputs
            HSOUTP               => dat_upLinkSerial_o,
            HSOUTN               => open,

            -- High speed deserializer inputs
            HSINP                => dat_downLinkSerial_i,
            HSINN                => dat_downLinkSerial_n_s,

            -- ePort clock differential outputs
            ECLK0P               => clk_eport0_o,
            ECLK0N               => open,

            ECLK1P               => clk_eport1_o,
            ECLK1N               => open,

            ECLK2P               => clk_eport2_o,
            ECLK2N               => open,

            ECLK3P               => clk_eport3_o,
            ECLK3N               => open,

            ECLK4P               => clk_eport4_o,
            ECLK4N               => open,

            ECLK5P               => clk_eport5_o,
            ECLK5N               => open,

            ECLK6P               => clk_eport6_o,
            ECLK6N               => open,

            ECLK7P               => clk_eport7_o,
            ECLK7N               => open,

            ECLK8P               => clk_eport8_o,
            ECLK8N               => open,

            ECLK9P               => clk_eport9_o,
            ECLK9N               => open,

            ECLK10P              => clk_eport10_o,
            ECLK10N              => open,

            ECLK11P              => clk_eport11_o,
            ECLK11N              => open,

            ECLK12P              => clk_eport12_o,
            ECLK12N              => open,

            ECLK13P              => clk_eport13_o,
            ECLK13N              => open,

            ECLK14P              => clk_eport14_o,
            ECLK14N              => open,

            ECLK15P              => clk_eport15_o,
            ECLK15N              => open,

            ECLK16P              => clk_eport16_o,
            ECLK16N              => open,

            ECLK17P              => clk_eport17_o,
            ECLK17N              => open,

            ECLK18P              => clk_eport18_o,
            ECLK18N              => open,

            ECLK19P              => clk_eport19_o,
            ECLK19N              => open,

            ECLK20P              => clk_eport20_o,
            ECLK20N              => open,

            ECLK21P              => clk_eport21_o,
            ECLK21N              => open,

            ECLK22P              => clk_eport22_o,
            ECLK22N              => open,

            ECLK23P              => clk_eport23_o,
            ECLK23N              => open,

            ECLK24P              => clk_eport24_o,
            ECLK24N              => open,

            ECLK25P              => clk_eport25_o,
            ECLK25N              => open,

            ECLK26P              => clk_eport26_o,
            ECLK26N              => open,

            ECLK27P              => clk_eport27_o,
            ECLK27N              => open,

            ECLK28P              => clk_eport28_s,
            ECLK28N              => open,

            -- ePortTX group 0 differential data outputs (downlink)
            EDOUT00P             => dat_eport0_o,
            EDOUT00N             => open,
            EDOUT01P             => dat_eport1_o,
            EDOUT01N             => open,
            EDOUT02P             => dat_eport2_o,
            EDOUT02N             => open,
            EDOUT03P             => dat_eport3_o,
            EDOUT03N             => open,

            -- ePortTX group 1 differential data outputs (downlink)
            EDOUT10P             => dat_eport10_o,
            EDOUT10N             => open,
            EDOUT11P             => dat_eport11_o,
            EDOUT11N             => open,
            EDOUT12P             => dat_eport12_o,
            EDOUT12N             => open,
            EDOUT13P             => dat_eport13_o,
            EDOUT13N             => open,

            -- ePortTX group 2 differential data outputs (downlink)
            EDOUT20P             => dat_eport20_o,
            EDOUT20N             => open,
            EDOUT21P             => dat_eport21_o,
            EDOUT21N             => open,
            EDOUT22P             => dat_eport22_o,
            EDOUT22N             => open,
            EDOUT23P             => dat_eport23_o,
            EDOUT23N             => open,

            -- ePortTX group 3 differential data outputs (downlink)
            EDOUT30P             => dat_eport30_o,
            EDOUT30N             => open,
            EDOUT31P             => dat_eport31_o,
            EDOUT31N             => open,
            EDOUT32P             => dat_eport32_o,
            EDOUT32N             => open,
            EDOUT33P             => dat_eport33_o,
            EDOUT33N             => open,

            -- ePortTX EC differential data outputs
            EDOUTECP             => dat_ec_tosca_s,
            EDOUTECN             => open,

            -- ePortRX group 0 differential data inputs (uplink)
            EDIN00P              => dat_eport0_i,
            EDIN00N              => dat_eport0_n_s,
            EDIN01P              => dat_eport1_i,
            EDIN01N              => dat_eport1_n_s,
            EDIN02P              => dat_eport2_i,
            EDIN02N              => dat_eport2_n_s,
            EDIN03P              => dat_eport3_i,
            EDIN03N              => dat_eport3_n_s,

            -- ePortRX group 1 differential data inputs (uplink)
            EDIN10P              => dat_eport10_i,
            EDIN10N              => dat_eport10_n_s,
            EDIN11P              => dat_eport11_i,
            EDIN11N              => dat_eport11_n_s,
            EDIN12P              => dat_eport12_i,
            EDIN12N              => dat_eport12_n_s,
            EDIN13P              => dat_eport13_i,
            EDIN13N              => dat_eport13_n_s,

            -- ePortRX group 2 differential data inputs (uplink)
            EDIN20P              => dat_eport20_i,
            EDIN20N              => dat_eport20_n_s,
            EDIN21P              => dat_eport21_i,
            EDIN21N              => dat_eport21_n_s,
            EDIN22P              => dat_eport22_i,
            EDIN22N              => dat_eport22_n_s,
            EDIN23P              => dat_eport23_i,
            EDIN23N              => dat_eport23_n_s,

            -- ePortRX group 3 differential data inputs (uplink)
            EDIN30P              => dat_eport30_i,
            EDIN30N              => dat_eport30_n_s,
            EDIN31P              => dat_eport31_i,
            EDIN31N              => dat_eport31_n_s,
            EDIN32P              => dat_eport32_i,
            EDIN32N              => dat_eport32_n_s,
            EDIN33P              => dat_eport33_i,
            EDIN33N              => dat_eport33_n_s,

            -- ePortRX group 4 differential data inputs (uplink)
            EDIN40P              => dat_eport40_i,
            EDIN40N              => dat_eport40_n_s,
            EDIN41P              => dat_eport41_i,
            EDIN41N              => dat_eport41_n_s,
            EDIN42P              => dat_eport42_i,
            EDIN42N              => dat_eport42_n_s,
            EDIN43P              => dat_eport43_i,
            EDIN43N              => dat_eport43_n_s,

            -- ePortRX group 5 differential data inputs (uplink)
            EDIN50P              => dat_eport50_i,
            EDIN50N              => dat_eport50_n_s,
            EDIN51P              => dat_eport51_i,
            EDIN51N              => dat_eport51_n_s,
            EDIN52P              => dat_eport52_i,
            EDIN52N              => dat_eport52_n_s,
            EDIN53P              => dat_eport53_i,
            EDIN53N              => dat_eport53_n_s,

            -- ePortRX group 6 differential data inputs (uplink)
            EDIN60P              => dat_eport60_i,
            EDIN60N              => dat_eport60_n_s,
            EDIN61P              => dat_eport61_i,
            EDIN61N              => dat_eport61_n_s,
            EDIN62P              => dat_eport62_i,
            EDIN62N              => dat_eport62_n_s,
            EDIN63P              => dat_eport63_i,
            EDIN63N              => dat_eport63_n_s,

            -- ePortRX EC differential data inputs
            EDINECP              => dat_ec_fromsca_s,
            EDINECN              => dat_ec_n_s,

            -- Phase shifted clocks
            PSCLK0P              => clk_psclk0_o,
            PSCLK0N              => open,
            PSCLK1P              => clk_psclk1_o,
            PSCLK1N              => open,
            PSCLK2P              => clk_psclk2_o,
            PSCLK2N              => open,
            PSCLK3P              => clk_psclk3_o,
            PSCLK3N              => open,

            -- I2C slave for ASIC control
            SLSDA                => SLSDA_s,       -- JM: Unused (future implementation)
            SLSCL                => SLSCL_s,       -- JM: Unused (future implementation)

            -- Address                          -- JM: Fixed (future implementation)
            ADR0                 => '0',
            ADR1                 => '0',
            ADR2                 => '0',
            ADR3                 => '0',

            -- lock mode
            LOCKMODE             => '0',

            -- reset (active low)
            RSTB                 => rst_lpgbtrstn_i, --'1', --rst_lpgbtrstn_i,

            -- reset signal
            RSTOUTB              => open,

            -- mode of operation
            MODE0                => conf_mode_s(0),
            MODE1                => conf_mode_s(1),
            MODE2                => conf_mode_s(2),
            MODE3                => conf_mode_s(3),

            -- lpGBT Ready signal
            READY                => stat_lpgbtrdy_o,

            -- Power On Reset Disable
            PORDIS               => '0',

            -- reference clock
            REFCLKP              => refclk_i,--'0',
            REFCLKN              => refclk_n,--'0',

            -- Test clock (used only for debugging)
            TSTCLKINP            => '1',
            TSTCLKINN            => '0',

            -- Bypass VCO and use test clock (TSTCLKIN) instead
            VCOBYPASS            => '0',

            -- State override for the power up state machine
            STATEOVRD            => '0',

            -- Selects configuration interface (0=SerialControll1=Slave I2C)
            SC_I2C               => '0',    -- JM: Enable IC interface

            -- Test outputs (0-3 CMOS 4-5 differential)
            TSTOUT0              => open,
            TSTOUT1              => open,
            TSTOUT2              => open,
            TSTOUT3              => open,
            TSTOUT4P             => open,
            TSTOUT4N             => open,
            TSTOUT5P             => open,
            TSTOUT5N             => open,

            -- I2C Master 0 signals
            M0SDA                => dat_i2cm0_sda,
            M0SCL                => clk_i2cm0_scl,

            -- I2C Master 1 signals
            M1SDA                => dat_i2cm1_sda,
            M1SCL                => clk_i2cm1_scl,

            -- I2C Master 2 signals
            M2SDA                => dat_i2cm2_sda,
            M2SCL                => clk_i2cm2_scl,

            -- Parallel I/O
            GPIO0                => dat_gpio0_io,
            GPIO1                => dat_gpio1_io,
            GPIO2                => dat_gpio2_io,
            GPIO3                => dat_gpio3_io,
            GPIO4                => dat_gpio4_io,
            GPIO5                => dat_gpio5_io,
            GPIO6                => dat_gpio6_io,
            GPIO7                => dat_gpio7_io,
            GPIO8                => dat_gpio8_io,
            GPIO9                => dat_gpio9_io,
            GPIO10               => dat_gpio10_io,
            GPIO11               => dat_gpio11_io,
            GPIO12               => dat_gpio12_io,
            GPIO13               => dat_gpio13_io,
            GPIO14               => dat_gpio14_io,
            GPIO15               => dat_gpio15_io,

            -- ADC (and current source output)
            ADC0                 => open,
            ADC1                 => open,
            ADC2                 => open,
            ADC3                 => open,
            ADC4                 => open,
            ADC5                 => open,
            ADC6                 => open,
            ADC7                 => open,

            -- Voltage DAC output
            VDAC                 => open,

            -- reference voltage
            VREF                 => open,

            -- debug signals (not present on the chip package)
            debug_registers     => open,
            debug_testOutputs   => open
        );

    --clk_eport28_s
    dat_ec_fromsca_s <= '0';

end behabioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
